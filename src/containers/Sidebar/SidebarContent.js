import React, {Component} from "react";
import {connect} from "react-redux";
import {Menu, Icon} from "antd";
import {Link} from "react-router-dom";

import CustomScrollbars from "util/CustomScrollbars";
import SidebarLogo from "./SidebarLogo";

import Auxiliary from "util/Auxiliary";
import UserProfile from "./UserProfile";
// import AppsNavigation from "./AppsNavigation";
import {
  NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  THEME_TYPE_LITE
} from "../../constants/ThemeSetting";
import IntlMessages from "../../util/IntlMessages";

// const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;


class SidebarContent extends Component {

  getNoHeaderClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR || navStyle === NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR) {
      return "gx-no-header-notifications";
    }
    return "";
  };
  getNavStyleSubMenuClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
      return "gx-no-header-submenu-popup";
    }
    return "";
  };

  render() {
    const {themeType, navStyle, pathname} = this.props;
    const selectedKeys = pathname.substr(1);
    const defaultOpenKeys = selectedKeys.split('/')[1];
    return (<Auxiliary>

        <SidebarLogo/>
      <div className="gx-sidebar-content">
          <div className={`gx-sidebar-notifications ${this.getNoHeaderClass(navStyle)}`}>
            <UserProfile/>
            {/* <AppsNavigation/> */}
          </div>
          <CustomScrollbars className="gx-layout-sider-scrollbar">
            <Menu
              defaultOpenKeys={[defaultOpenKeys]}
              selectedKeys={[selectedKeys]}
              theme={themeType === THEME_TYPE_LITE ? 'lite' : 'dark'}
              mode="inline">

            <MenuItemGroup key="dashboard" className="gx-menu-group" title={<span><Icon type="dashboard" /> <IntlMessages id="sidebar.group.dashboard" /></span>}>
              <Menu.Item key="main">
                <Link to="/dashboard/main">
                  Dashboard
                </Link>
              </Menu.Item>
              <Menu.Item key="gisKPLP">
                <Link to="/gisKPLP">
                  <IntlMessages id="sidebar.gisKPLP" />
                </Link>
              </Menu.Item>
            </MenuItemGroup>

            <MenuItemGroup key="simKPLP" className="gx-menu-group" title={<span><Icon type="appstore" /> <IntlMessages id="sidebar.group.simKPLP" /></span>}>
                <Menu.Item key="tupoksi">
                  <Link to="/simKPLP/tupoksi">
                    <IntlMessages id="sidebar.tupoksi" />
                  </Link>
                </Menu.Item>
                <Menu.Item key="hukumNasional">
                <Link to="/simKPLP/landasanhukumnasional">
                    <IntlMessages id="sidebar.landasanHukumNasional" />
                  </Link>
                </Menu.Item>
                <Menu.Item key="hukumInternasional">
                <Link to="/simKPLP/landasanhukuminternasional">
                    <IntlMessages id="sidebar.landasanHukumInternasional" />
                  </Link>
                </Menu.Item>
                <Menu.Item key="publikasi">
                  <Link to="/simKPLP/publikasi">
                    <IntlMessages id="sidebar.publikasi" />
                  </Link>
                </Menu.Item>
                <Menu.Item key="maklumatPelayaran">
                  <Link to="/simKPLP/maklumatpelayaran">
                    <IntlMessages id="sidebar.maklumatPelayaran" />
                  </Link>
                </Menu.Item>
              </MenuItemGroup>

            <MenuItemGroup key="kegiatanProgram" className="gx-menu-group" title={<span><Icon type="schedule" /> <IntlMessages id="sidebar.group.kegiatanProgram" /></span>}>
              <Menu.Item key="kegiatan">
              <Link to="/kegiatan-program/data-kegiatan">
                  <IntlMessages id="sidebar.dataKegiatan" />
                </Link>
              </Menu.Item>
              <Menu.Item key="timeline">
                <Link to="/kegiatan-program/timeline">
                  <IntlMessages id="sidebar.timeline" />
                </Link>
              </Menu.Item>
            </MenuItemGroup>
  
            <MenuItemGroup key="masterdata" className="gx-menu-group" title={<span><Icon type="setting" /> Master Data</span>}>
              <Menu.Item key="mcio">
                <Link to="/masterdata/mcio">
                  Data MCIO
                </Link>
              </Menu.Item>
              <Menu.Item key="psco">
                <Link to="/masterdata/psco">
                  Data PSCO
                </Link>
              </Menu.Item>
            </MenuItemGroup>

            </Menu>
          </CustomScrollbars>
        </div>
      </Auxiliary>
    );
  }
}

SidebarContent.propTypes = {};
const mapStateToProps = ({settings}) => {
  const {navStyle, themeType, locale, pathname} = settings;
  return {navStyle, themeType, locale, pathname}
};
export default connect(mapStateToProps)(SidebarContent);


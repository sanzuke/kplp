import React, { Component } from "react";
import { connect } from "react-redux";
import { Avatar, Popover } from "antd";
import { userSignOut } from "appRedux/actions/Auth";

class UserProfile extends Component {

  render() {
    const userMenuOptions = (
      <ul className="gx-user-popover">
        <li>
          <Avatar icon="user" />
        </li>
        <li>Profil</li>
        <li>Ubah Password</li>
        <li onClick={() => this.props.userSignOut()}>Logout
        </li>
      </ul>
    );

    return (

      <div className="gx-flex-row gx-align-items-center gx-mb-4 gx-avatar-row" style={{ marginLeft: "30px", textAlign: "center" }}>
        <Popover placement="rightTop" content={userMenuOptions} trigger="click">
          <Avatar size={64} icon="user" />
          {/* <i className="icon icon-chevron-down gx-fs-xxs gx-ml-2" /> */}
          <span className="gx-avatar-name">
            <div>Mulyadi</div>
            <div style={{ color: "#cccccc", fontSize: "13px" }}>System Administrator</div>
            <div style={{ color: "#cccccc", fontSize: "13px" }}>Direktorat KPLP</div></span>
        </Popover>
      </div>

    )

  }
}

export default connect(null, { userSignOut })(UserProfile);

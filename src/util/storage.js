export const loadState = () => {
  try {
    const serializedState = sessionStorage.getItem('persisted')

    if (serializedState !== null) return JSON.parse(serializedState)

    return {}
  } catch (error) {
    console.log('Error loading state from local storage: ') // eslint-disable-line no-console
    console.log(error) // eslint-disable-line no-console
    return {}
  }
}

export const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state)

    // localStorage.setItem('state', serializedState)
    sessionStorage.setItem('persisted', serializedState)
  } catch (error) {
    console.log('Error saving state to local storage') // eslint-disable-line no-console
    console.log(error) // eslint-disable-line no-console
  }
}

import { API } from './config'

export default function (resource) {
  const method = API.SECURE ? 'https' : 'http'

  // let url = `${method}://${API.URL}:${API.PORT}${API.PATH}`
  let url = `${method}://${API.URL}:${API.PORT}`

  if (resource) url += `/${resource}`

  return url
}

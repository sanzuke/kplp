import antdID from "antd/lib/locale-provider/id_ID";
import appLocaleData from "react-intl/locale-data/id";
import enMessages from "../locales/id_ID.json";

const IdLang = {
  messages: {
    ...enMessages
  },
  antd: antdID,
  locale: 'id-ID',
  data: appLocaleData
};
export default IdLang;

import React from "react";
import { Route, Switch } from "react-router-dom";

import asyncComponent from "util/asyncComponent";

import Main from "./dashboard/index";
import simKPLP from "./simKPLP/index";
import kegiatanProgram from "./KegiatanProgram/index";
import masterdata from "./masterdata/index";

const App = ({ match }) => (
  <div className="gx-main-content-wrapper">
    <Switch>
      <Route path={`${match.url}dashboard`} component={Main} />
      <Route path={`${match.url}sample`} component={asyncComponent(() => import('./SamplePage'))} />
      <Route path={`${match.url}simKPLP`} component={simKPLP} />
      <Route path={`${match.url}kegiatan-program`} component={kegiatanProgram} />
      <Route path={`${match.url}masterdata`} component={masterdata} />
    </Switch>
  </div>
);

export default App;
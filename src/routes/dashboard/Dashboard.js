import React from "react";
import { Col, Row, Table, Card } from "antd";

// import { Area, AreaChart, Line, LineChart, ResponsiveContainer, Tooltip } from "recharts";
import Auxiliary from "util/Auxiliary";
// import Portfolio from "components/dashboard/Crypto/Portfolio";
// import BalanceHistory from "components/dashboard/Crypto/BalanceHistory";
// import SendMoney from "components/dashboard/Crypto/SendMoney";
// import RewardCard from "components/dashboard/Crypto/RewardCard";
// import CurrencyCalculator from "components/dashboard/Crypto/CurrencyCalculator";
// import CryptoNews from "components/dashboard/Crypto/CryptoNews";
// import DownloadMobileApps from "components/dashboard/Crypto/DownloadMobileApps";
import OrderHistory from "components/dashboard/Crypto/OrderHistory";

const Dashboard = () => {
  return (
    <Auxiliary>
      <Row>
        <Col xl={16} lg={12} md={12} sm={18} xs={24}>
          <h2>Jadwal Kegiatan Bulan Ini</h2>
          <OrderHistory />
        </Col>
        <Col xl={8} lg={12} md={12} sm={6} xs={24}>
          <Card title="Penetapan SSO" extra={220} style={{ width: 300 }}>
            <p>100 Baru</p>
            <p>120 Perpanjang</p>
          </Card>
          <Card title="Total Senjata Api" extra={1.156} style={{ width: 300 }}>
            <p>1.000 Kondisi Baik</p>
            <p>156 Kondisi Rusak</p>
          </Card>
        </Col>

        <Col xl={8} lg={12} md={12} sm={6} xs={24}>
          
        </Col>
      </Row>
    </Auxiliary>
  );
};

export default Dashboard;

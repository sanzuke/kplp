import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import asyncComponent from "util/asyncComponent";

const dashboard = ({match}) => (
  <Switch>
    <Route path={`${match.url}/main`} component={asyncComponent(() => import('./Dashboard'))} />
  </Switch>
);

export default dashboard;

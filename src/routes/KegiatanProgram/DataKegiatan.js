import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Button, Card, Form, Input, Checkbox, DatePicker, Select, Upload, Icon, Table, Divider } from "antd";
import { convertToRaw, EditorState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

const FormItem = Form.Item;
const Option = Select.Option;
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;


const props = {
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  onChange({ file, fileList }) {
    if (file.status !== 'uploading') {
      console.log(file, fileList);
    }
  },
};

const dataSource = [
  {
    no: '1',
    kategori: 'Surat Edaran',
    perihal: "Lorem ipsum",
    deskripsi: "Lorem ipsum",
    file: 'file.pdf',
  },
  {
    no: '2',
    kategori: 'Intruksi',
    perihal: "Lorem ipsum",
    deskripsi: "Lorem ipsum",
    file: 'file.pdf',
  },
  {
    no: '3',
    kategori: 'Surat Edaran',
    perihal: "Lorem ipsum",
    deskripsi: "Lorem ipsum",
    file: 'file.pdf',
  },
  {
    no: '4',
    kategori: 'Telegram',
    perihal: "Lorem ipsum",
    deskripsi: "Lorem ipsum",
    file: 'file.pdf',
  },
];

const columns = [
  {
    title: 'No',
    dataIndex: 'no',
    key: 'no',
  },
  {
    title: 'Kategori',
    dataIndex: 'kategori',
    key: 'kategori',
  },
  {
    title: 'Perihal',
    dataIndex: 'perihal',
    key: 'perihal',
  },
  {
    title: 'Deskripsi / Narasi / Isi',
    dataIndex: 'deskripsi',
    key: 'deskripsi',
  },
  {
    title: 'File',
    dataIndex: 'file',
    key: 'file',
  },
  {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
      <span>
        <a href="javascript:;">view</a>
        <Divider type="vertical" />
        <a href="javascript:;">Ubah</a>
        <Divider type="vertical" />
        <a href="javascript:;">Hapus</a>
      </span>
    ),
  },
];

class DataKegiatan extends PureComponent {

  constructor() {
    super();
    this.state = {
      formLayout: 'horizontal',
    };
  }

  onChange = (date, dateString) => {
    console.log(date, dateString)
  }

  render() {
    const { formLayout } = this.state;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = formLayout === 'horizontal' ? {
      labelCol: { xs: 24, sm: 6 },
      wrapperCol: { xs: 24, sm: 14 },
    } : null;
    const buttonItemLayout = formLayout === 'horizontal' ? {
      wrapperCol: { xs: 24, sm: { span: 14, offset: 6 } },
    } : null;

    return (
      <div>
        <Card className="gx-card" title="Input Kegiatan & Program">
          <Form layout={formLayout}>

            <FormItem
              {...formItemLayout}
              label="Jenis Kegiatan"
              hasFeedback
            >
              {getFieldDecorator('jenis', {
                rules: [
                  { required: true, message: 'Silahkan pilih jenis kegiatan' },
                ],
              })(
                <Select placeholder="Silahkan pilih jenis kegiatan">
                  <Option value="surat_edaran">Surat Edaran</Option>
                  <Option value="intruksi">Instruksi</Option>
                  <Option value="telegram">Telegram</Option>
                </Select>
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Tanggal kegiatan"
              hasFeedback
            >
              {getFieldDecorator('tanggal', {
                rules: [
                  { required: true, message: 'Silahkan pilih tanggal maklumat' },
                ],
              })(
                <RangePicker onChange={this.onChange} />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Judul Kegiatan"
              hasFeedback
            >
              {getFieldDecorator('judul', {
                rules: [
                  { required: true, message: 'Silahkan isi judul kegiatan' },
                ],
              })(
                <Input type="text" />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Lokasi / Tempat"
              hasFeedback
            >
              {getFieldDecorator('lokasi_tempat', {
                rules: [
                  { required: true, message: 'Silahkan isi lokasi / tempat' },
                ],
              })(
                <Input type="text" />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Deskripsi Kegiatan"
              hasFeedback
            >
              {getFieldDecorator('dari', {
                rules: [
                  { required: true, message: 'Silahkan isi dari' },
                ],
              })(
                <Input type="text" />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="PIC Kegiatan"
              hasFeedback
            >
              {getFieldDecorator('pic_kegiatan', {
                rules: [
                  { required: true, message: 'Silahkan isi kepada' },
                ],
              })(
                <Input type="text" />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Upload Lampiran"
              hasFeedback
            >
              {getFieldDecorator('select', {
                rules: [
                  { required: true, message: 'Upload file' },
                ],
              })(
                <Upload {...props}>
                  <Button><Icon type="upload" /> Upload</Button>
                </Upload>
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Upload Foto"
              hasFeedback
            >
              {getFieldDecorator('select', {
                rules: [
                  { required: true, message: 'Upload file' },
                ],
              })(
                <Upload {...props}>
                  <Button><Icon type="upload" /> Upload</Button>
                </Upload>
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Pagu Anggaran Tahunan"
              hasFeedback
            >
              {getFieldDecorator('anggaran_tahunan', {
                rules: [
                  { required: true, message: 'Silahkan isi kepada' },
                ],
              })(
                <Input type="text" />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Realisasi Penggunaan"
              hasFeedback
            >
              {getFieldDecorator('realisasi', {
                rules: [
                  { required: true, message: 'Silahkan isi kepada' },
                ],
              })(
                <Input type="text" />
              )}
            </FormItem>

            <Form.Item {...formItemLayout} hasFeedback>
              {getFieldDecorator('telah_dilaksanakan', {
                valuePropName: 'checked',
              })(
                <Checkbox>
                  Kegiatan Telah Dilaksanakan
                </Checkbox>
              )}
            </Form.Item>

            <FormItem {...buttonItemLayout}>
              <Button type="primary">Update</Button>
            </FormItem>
          </Form>
        </Card>

        <Card className="gx-card" title="Daftar Maklumat Pelayaran">
          <Table dataSource={dataSource} columns={columns} />
        </Card>

      </div>
    );
  }
}

DataKegiatan.propTypes = {

};
const DataKegiatans = Form.create()(DataKegiatan);
export default DataKegiatans;
import React from "react";
import { Route, Switch } from "react-router-dom";
import asyncComponent from "util/asyncComponent";

const KegiatanProgram = ({ match }) => (
  <Switch>
    <Route path={`${match.url}/data-kegiatan`} component={asyncComponent(() => import('./DataKegiatan'))} />
    <Route path={`${match.url}/timeline`} component={asyncComponent(() => import('./Timeline'))} />
  </Switch>
);

export default KegiatanProgram;

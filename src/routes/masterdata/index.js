import React from "react";
import {Route, Switch} from "react-router-dom";
import asyncComponent from "util/asyncComponent";

const masterdata = ({match}) => (
  <Switch>
    <Route path={`${match.url}/mcio`} component={asyncComponent(() => import('./listMCIO'))} />
    <Route path={`${match.url}/psco`} component={asyncComponent(() => import('./listPSCO'))} />
  </Switch>
);

export default masterdata;

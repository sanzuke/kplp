import React, { Component } from "react";
import { Button, Card, Form, Select, Spin, notification } from "antd";
import { EditorState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

import { connect } from "react-redux";
// saga
import { fetchDirektorat, fetchSeksi, postTupoksi, resetGeneral } from "appRedux/general/actions"
import { showAuthLoader } from "appRedux/actions/Auth";

const FormItem = Form.Item;
const Option = Select.Option;

class Tupoksi extends Component {
  
  state = {
    editorStateFungsi: EditorState.createEmpty(),
    editorStateTugas: EditorState.createEmpty(),
    onSubmit: false,
  };

  componentDidMount() {
    const { resetGeneral } = this.props
    resetGeneral();
    this.fetchDirektorat()
    this.fetchSeksi()
  }

  onEditorStateChangeTugas = (editorState) => {
    this.setState({
      editorStateTugas: editorState,
    });
  };

  onEditorStateChangeFungsi = (editorState) => {
    this.setState({
      editorStateFungsi: editorState,
    });
  };

  handleFormLayoutChange = (e) => {
    this.setState({ formLayout: e.target.value });
  }

  fetchDirektorat = () => {
    const { fetchDirektorat } = this.props
    // console.log("fetch direktorat");
    
    fetchDirektorat("1");
  }

  fetchSeksi = () => {
    const { fetchSeksi } = this.props
    // console.log("fetch seksi");

    fetchSeksi();
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { postTupoksi, form } = this.props
    form.validateFields((err, values) => {
      if (!err) {
        this.props.showAuthLoader();
        values.created_by = localStorage.getItem("user_id")
        values.tugas = values.tugas.blocks[0].text
        values.fungsi = values.fungsi.blocks[0].text
        postTupoksi(values);
      }
      // console.log(values);
      this.setState({onSubmit: true})
    });
  }

  openNotificationWithIcon = (type, message) => {
    notification[type]({
      message: 'Simpan Data',
      description: message,
    });
  };

  constructor() {
    super();
    this.state = {
      formLayout: 'horizontal',
    };
  }

  render() {
    const { editorStateTugas, editorStateFungsi, formLayout, onSubmit } = this.state;
    const { direktorat, seksi, isLoading, showMessage, alertMessage } = this.props;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = formLayout === 'horizontal' ? {
      labelCol: { xs: 24, sm: 6 },
      wrapperCol: { xs: 24, sm: 14 },
    } : null;
    const buttonItemLayout = formLayout === 'horizontal' ? {
      wrapperCol: { xs: 24, sm: { span: 14, offset: 6 } },
    } : null;

    // console.log(showMessage);
    // console.log(onSubmit);
    if (showMessage && onSubmit){
      this.openNotificationWithIcon('success', alertMessage)
      this.setState({onSubmit: false})
    }
    return (
      <Spin spinning={isLoading}>
        <Card className="gx-card" title="Input Tupoksi">
          <Form onSubmit={this.handleSubmit} layout={formLayout}>
            
            <FormItem
              {...formItemLayout}
              label="Sub Direktorat"
              hasFeedback
            >
              {getFieldDecorator('direktorat_id', {
                rules: [
                  { required: true, message: 'Silahkan pilih Sub Direktorat' },
                ],
              })(
                <Select placeholder="Silahkan pilih Sub Direktorat">
                  {direktorat !== undefined && direktorat !== null && direktorat.data !== undefined && direktorat.data.map((v) => (
                    <Option key={v.id} value={v.id}>{v.nama}</Option>
                  ))}
                </Select>
              )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="Seksi"
              hasFeedback
            >
              {getFieldDecorator('seksi_id', {
                rules: [
                  { required: true, message: 'Silahkan pilih Seksi' },
                ],
              })(
                <Select placeholder="Silahkan pilih Seksi">
                  { seksi !== undefined && seksi !== null && seksi.data !== undefined && seksi.data.map((v) => (
                    <Option key={v.id} value={v.id}>{v.nama}</Option>
                  ))}
                </Select>
              )}
            </FormItem>
            <FormItem {...formItemLayout}
              label="Tugas"
              // hasFeedback
            >
              {getFieldDecorator('tugas', {
                rules: [
                  { required: true, message: 'Silahkan isi tugas' },
                ],
              })(
                <Editor editorStyle={{
                  width: '100%',
                  minHeight: 100,
                  borderWidth: 1,
                  borderStyle: 'solid',
                  borderColor: 'lightgray',
                  
                }}
                  editorState={editorStateTugas}
                  wrapperClassName="demo-wrapper"
                  onEditorStateChange={this.onEditorStateChangeTugas}
                  toolbarClassName="toolbar-class"
                />
              )}
              {/* <textarea style={{ width: '100%', height: 200 }}
                disabled
                value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
              /> */}
            </FormItem>

            <FormItem {...formItemLayout}
              label="Fungsi"
              // hasFeedback
            >
              {getFieldDecorator('fungsi', {
                rules: [
                  { required: true, message: 'Silahkan isi fungsi' },
                ],
              })(
                <Editor editorStyle={{
                  width: '100%',
                  minHeight: 100,
                  borderWidth: 1,
                  borderStyle: 'solid',
                  borderColor: 'lightgray'
                }}
                  editorState={editorStateFungsi}
                  wrapperClassName="demo-wrapper"
                  onEditorStateChange={this.onEditorStateChangeFungsi}
                  minHeight={"200px"}
                />
              )}
              {/* <textarea style={{ width: '100%', height: 200 }}
                disabled
              value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
              /> */}
            </FormItem>
            
            <FormItem {...buttonItemLayout}>
              <Button type="primary" htmlType="submit">Update</Button>
            </FormItem>
          </Form>
        </Card>
      </Spin>
    );
  }

}
const Tupoksis = Form.create()(Tupoksi);

const mapStateToProps = (state) => ({
  http: state.http,
  isFetching: state.http.isFetching,
  direktorat: state.general.subditCollection,
  seksi: state.general.seksiCollection,
  resultsTupoksi: state.general.results,
  isSuccess: state.general.isSuccess,
  showMessage: state.general.showMessage,
  alertMessage: state.general.alertMessage,
  isLoading: state.general.loader,
})

export default connect(mapStateToProps, {
  fetchDirektorat,
  fetchSeksi,
  postTupoksi,
  resetGeneral,
  showAuthLoader
})(Tupoksis);

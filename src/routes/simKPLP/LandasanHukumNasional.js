import React, { PureComponent } from 'react';
// import PropTypes from 'prop-types';
import { Button, Card, Form, Input, Radio, Select, Upload, Icon, Table, Divider, Modal, Row, Col } from "antd";
// import IntlMessages from "util/IntlMessages";
import SearchBox from '../../components/SearchBox'

import { connect } from "react-redux";
// saga
import {
  fetchLandasanHukumInt,
  setLandasanHukumInt,
  postLandasanHukumInt
} from "appRedux/general/actions"
import { showAuthLoader } from "appRedux/actions/Auth";

const FormItem = Form.Item;
const Option = Select.Option;

class LandasanHukumNasional extends PureComponent {

  constructor() {
    super();
    this.state = {
      formLayout: 'horizontal',
      visible: false,
      kategori: '',
      uploading: false,
      fileList: [],
    };
  }

  componentDidMount() {
    this.fetch()
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  fetch = () => {
    const { fetchLandasanHukumInt } = this.props
    fetchLandasanHukumInt({ jenis: "Nasional" })
  }

  render() {
    const { resultsLandasanHukum, isFetching } = this.props
    const { getFieldDecorator } = this.props.form;
    const { formLayout, fileList } = this.state;
    const formItemLayout = formLayout === 'horizontal' ? {
      labelCol: { xs: 24, sm: 6 },
      wrapperCol: { xs: 24, sm: 14 },
    } : null;
    const buttonItemLayout = formLayout === 'horizontal' ? {
      wrapperCol: { xs: 24, sm: { span: 14, offset: 6 } },
    } : null;

    const columns = [
      {
        title: 'Kategori',
        dataIndex: 'kategori',
        key: 'kategori',
      },
      {
        title: 'Deskripsi',
        dataIndex: 'deskripsi',
        key: 'deskripsi',
      },
      {
        title: 'File',
        dataIndex: 'file',
        key: 'file',
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <span>
            <a href="javascript:;">Ubah</a>
            <Divider type="vertical" />
            <a href="javascript:;">Hapus</a>
          </span>
        ),
      },
    ];

    const props = {
      onRemove: (file) => {
        this.setState((state) => {
          const index = state.fileList.indexOf(file);
          const newFileList = state.fileList.slice();
          newFileList.splice(index, 1);
          return {
            fileList: newFileList,
          };
        });
      },
      beforeUpload: (file) => {
        this.setState((state) => ({
          fileList: [...state.fileList, file],
        }));
        return false;
      },
      fileList,
    };

    return (
      <div>
        <Modal
          title="Tambah"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          okText="Simpan"
          cancelText="Batal"
          width="70%"
        >
          <Form layout={formLayout}>
            <FormItem
              {...formItemLayout}
              label="Kategori Aturan"
              hasFeedback
            >
              {getFieldDecorator('select', {
                rules: [
                  { required: true, message: 'Silahkan pilih kategori aturan' },
                ],
              })(
                <Radio.Group name="radiogroup" value={this.state.kategori}>
                  <Radio value="UU">UU</Radio>
                  <Radio value="PP">PP</Radio>
                  <Radio value="PM">PM</Radio>
                  <Radio value="SK DIRJEN">SK DIRJEN</Radio>
                </Radio.Group>
              )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="Deskripsi Landasan Hukum Nasional"
              hasFeedback
            >
              {getFieldDecorator('select', {
                rules: [
                  { required: true, message: 'Masukan deskripsi landasan hukum' },
                ],
              })(
                <Input type="text" />
              )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="Upload Landasan Humum"
              hasFeedback
            >
              {getFieldDecorator('select', {
                rules: [
                  { required: true, message: 'Upload file landasan hukum' },
                ],
              })(
                <Upload {...props}>
                  <Button><Icon type="upload" /> Upload</Button>
                </Upload>
              )}
            </FormItem>
            {/* <FormItem {...buttonItemLayout}>
              <Button type="primary">Update</Button>
            </FormItem> */}
          </Form>
        </Modal>

        <Card className="gx-card" title="Daftar Landasan Hukum Nasional">
          <Row gutter={16}>
            <Col className="gutter-row" span={20}>
              {/* <SearchBox placeholder="Cari" /> */}
            </Col>
            <Col className="gutter-row" span={4}>
              <div style={{ marginBottom: 16 }}>
                <Button type="primary" onClick={this.showModal} block >
                  <Icon type="plus" /> Tambah
                </Button>
              </div>
            </Col>
          </Row>
          <Table
            className="gx-table-responsive"
            loading={isFetching}
            onChange={this.handleChange}
            dataSource={(resultsLandasanHukum !== undefined ? resultsLandasanHukum.data : [])}
            columns={columns}
          />
        </Card>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  http: state.http,
  isFetching: state.http.isFetching,
  resultsLandasanHukum: state.general.results,
  isSuccess: state.general.isSuccess,
  showMessage: state.general.showMessage,
  alertMessage: state.general.alertMessage,
  isLoading: state.general.loader,
})
const LandasanHukumNasionals = Form.create()(LandasanHukumNasional);
export default connect(mapStateToProps, {
  fetchLandasanHukumInt,
  setLandasanHukumInt,
  postLandasanHukumInt,
  showAuthLoader
})(LandasanHukumNasionals);

import React, { PureComponent } from 'react';
// import PropTypes from 'prop-types';
import { Button, Card, Form, Input, DatePicker, Select, Upload, Icon, Table, Divider } from "antd";
// import { convertToRaw, EditorState } from "draft-js";
// import { Editor } from "react-draft-wysiwyg";
// import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

const FormItem = Form.Item;
// const Option = Select.Option;

const props = {
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  onChange({ file, fileList }) {
    if (file.status !== 'uploading') {
      console.log(file, fileList);
    }
  },
  defaultFileList: [
    {
      uid: '1',
      name: 'xxx.png',
      status: 'done',
      response: 'Server Error 500', // custom error message to show
      url: 'http://www.baidu.com/xxx.png',
    },
    {
      uid: '2',
      name: 'yyy.png',
      status: 'done',
      url: 'http://www.baidu.com/yyy.png',
    },
    {
      uid: '3',
      name: 'zzz.png',
      status: 'error',
      response: 'Server Error 500', // custom error message to show
      url: 'http://www.baidu.com/zzz.png',
    },
  ],
};

const dataSource = [
  {
    no: '1',
    kategori: 'Surat Edaran',
    perihal: "Lorem ipsum",
    deskripsi: "Lorem ipsum",
    file: 'file.pdf',
  },
  {
    no: '2',
    kategori: 'Intruksi',
    perihal: "Lorem ipsum",
    deskripsi: "Lorem ipsum",
    file: 'file.pdf',
  },
  {
    no: '3',
    kategori: 'Surat Edaran',
    perihal: "Lorem ipsum",
    deskripsi: "Lorem ipsum",
    file: 'file.pdf',
  },
  {
    no: '4',
    kategori: 'Telegram',
    perihal: "Lorem ipsum",
    deskripsi: "Lorem ipsum",
    file: 'file.pdf',
  },
];

const columns = [
  {
    title: 'No',
    dataIndex: 'no',
    key: 'no',
  },
  {
    title: 'Kategori',
    dataIndex: 'kategori',
    key: 'kategori',
  },
  {
    title: 'Perihal',
    dataIndex: 'perihal',
    key: 'perihal',
  },
  {
    title: 'Deskripsi / Narasi / Isi',
    dataIndex: 'deskripsi',
    key: 'deskripsi',
  },
  {
    title: 'File',
    dataIndex: 'file',
    key: 'file',
  },
  {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
      <span>
        <a href="javascript:;">view</a>
        <Divider type="vertical" />
        <a href="javascript:;">Ubah</a>
        <Divider type="vertical" />
        <a href="javascript:;">Hapus</a>
      </span>
    ),
  },
];

class MaklumatPelayaran extends PureComponent {

  constructor() {
    super();
    this.state = {
      formLayout: 'horizontal',
    };
  }

  onChange = (date, dateString) => {
    console.log(date, dateString)
  }

  render() {
    const { formLayout } = this.state;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = formLayout === 'horizontal' ? {
      labelCol: { xs: 24, sm: 6 },
      wrapperCol: { xs: 24, sm: 14 },
    } : null;
    const buttonItemLayout = formLayout === 'horizontal' ? {
      wrapperCol: { xs: 24, sm: { span: 14, offset: 6 } },
    } : null;

    return (
      <div>
        <Card className="gx-card" title="Input Publikasi">
          <Form layout={formLayout}>

            <FormItem
              {...formItemLayout}
              label="Tanggal Maklumat"
              hasFeedback
            >
              {getFieldDecorator('tanggal', {
                rules: [
                  { required: true, message: 'Silahkan pilih tanggal maklumat' },
                ],
              })(
                <DatePicker onChange={this.onChange} />
              )}
            </FormItem>
              
            <FormItem
              {...formItemLayout}
              label="Nomor"
              hasFeedback
            >
              {getFieldDecorator('nomor', {
                rules: [
                  { required: true, message: 'Silahkan isi nomor' },
                ],
              })(
                <Input type="text" />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Perihal"
              hasFeedback
            >
              {getFieldDecorator('perihal', {
                rules: [
                  { required: true, message: 'Silahkan isi perihal' },
                ],
              })(
                <Input type="text" />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Dari"
              hasFeedback
            >
              {getFieldDecorator('dari', {
                rules: [
                  { required: true, message: 'Silahkan isi dari' },
                ],
              })(
                <Input type="text" />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Kepada"
              hasFeedback
            >
              {getFieldDecorator('kepada', {
                rules: [
                  { required: true, message: 'Silahkan isi kepada' },
                ],
              })(
                <Input type="text" />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Upload file"
              hasFeedback
            >
              {getFieldDecorator('select', {
                rules: [
                  { required: true, message: 'Upload file' },
                ],
              })(
                <Upload {...props}>
                  <Button><Icon type="upload" /> Upload</Button>
                </Upload>
              )}
            </FormItem>

            <FormItem {...buttonItemLayout}>
              <Button type="primary">Update</Button>
            </FormItem>
          </Form>
        </Card>

        <Card className="gx-card" title="Daftar Maklumat Pelayaran">
          <Table dataSource={dataSource} columns={columns} />
        </Card>

      </div>
    );
  }
}

MaklumatPelayaran.propTypes = {

};
const MaklumatPelayarans = Form.create()(MaklumatPelayaran);
export default MaklumatPelayarans;
import React, { PureComponent } from 'react';
// import PropTypes from 'prop-types';
import { Button, Card, Form, Input, Radio, Modal, Upload, Icon, Table, Divider, Row, Col, Checkbox, message } from "antd";
// import IntlMessages from "util/IntlMessages";
import SearchBox from '../../components/SearchBox'

import { connect } from "react-redux";
// saga
import {
  fetchLandasanHukumInt,
  setLandasanHukumInt,
  postLandasanHukumInt
} from "appRedux/general/actions"
import { showAuthLoader } from "appRedux/actions/Auth";
import CircularProgress from "components/CircularProgress/index";

const FormItem = Form.Item;

class LandasanHukumInternasional extends PureComponent {

  constructor() {
    super();
    this.state = {
      formLayout: 'horizontal',
      visible: false,
      kategori: '',
      onSubmit: false,
      filteredInfo: null,
      sortedInfo: null,
      uploading: false,
      fileList: [],
    };
  }

  componentDidMount() {
    this.fetch()
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = (e) => {
    // console.log(e);
    this.setState({
      visible: false,
    });
  };

  fetch = () => {
    const { fetchLandasanHukumInt } = this.props
    fetchLandasanHukumInt({jenis: "Internasional" })
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { postLandasanHukumInt, form } = this.props
    form.validateFields((err, values) => {
      if (!err) {
        let data = new FormData()
        this.props.showAuthLoader();
        data.append('created_by', localStorage.getItem("user_id"))
        data.append('jenis', 'Internasional')
        data.append('kategori', values.kategori)
        data.append('deskripsi', values.deskripsi)
        data.append('seksi_id', localStorage.getItem("seksi_id"))
        data.append('landasan_file', this.state.fileList[0])
        
        postLandasanHukumInt(data, () => {
          console.log("callback function");
          
        });
      }
      this.setState({ onSubmit: true })
    });
  }

  handleChange = (pagination, filters, sorter) => {
    console.log('Various parameters', pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  render() {
    console.log(this.props);
    const { resultsLandasanHukum, isFetching, alertMessage, showMessage } = this.props
    const { getFieldDecorator } = this.props.form;
    const { formLayout, onSubmit, fileList, uploading } = this.state;
    const formItemLayout = formLayout === 'horizontal' ? {
      labelCol: { xs: 24, sm: 8 },
      wrapperCol: { xs: 24, sm: 14 },
    } : null;
    // const buttonItemLayout = formLayout === 'horizontal' ? {
    //   wrapperCol: { xs: 24, sm: { span: 14, offset: 6 } },
    // } : null;

    const props = {
      onRemove: (file) => {
        this.setState((state) => {
          const index = state.fileList.indexOf(file);
          const newFileList = state.fileList.slice();
          newFileList.splice(index, 1);
          return {
            fileList: newFileList,
          };
        });
      },
      beforeUpload: (file) => {
        this.setState((state) => ({
          fileList: [...state.fileList, file],
        }));
        return false;
      },
      fileList,
    };

    const columns = [
      {
        title: 'Kategori',
        dataIndex: 'kategori',
        key: 'kategori',
      },
      {
        title: 'Deskripsi',
        dataIndex: 'deskripsi',
        key: 'deskripsi',
      },
      {
        title: 'File',
        dataIndex: 'landasan_file',
        key: 'file',
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <span>
            <a href="javascript:;">Ubah</a>
            <Divider type="vertical" />
            <a href="javascript:;">Hapus</a>
          </span>
        ),
      },
    ];

    return (
      <div>
        <Modal
          title="Tambah"
          visible={this.state.visible}
          onOk={this.handleSubmit}
          onCancel={this.handleCancel}
          okText="Simpan"
          cancelText="Batal"
          width="70%"
        >
          <Form onSubmit={this.handleSubmit} layout={formLayout}>
            <FormItem
              {...formItemLayout}
              label="Kategori Aturan"
              // hasFeedback
            >
              {getFieldDecorator('kategori', {
                rules: [
                  { required: true, message: 'Silahkan pilih kategori aturan' },
                ],
              })(
                <Radio.Group name="radiogroup"  >
                  <Radio value="KK">Kecelakaan Kapal</Radio>
                  <Radio value="PKA">Pemeriksaan Kapal Asing</Radio>
                </Radio.Group>
              )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="Deskripsi Landasan Hukum Internasional"
              hasFeedback
            >
              {getFieldDecorator('deskripsi', {
                rules: [
                  { required: true, message: 'Masukan deskripsi landasan hukum' },
                ],
              })(
                <Input type="text" />
              )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="Upload Landasan Humum"
              hasFeedback
            >
              {/* {getFieldDecorator('landasan_file', {
                rules: [
                  { message: 'Upload file landasan hukum', type: "file" },
                ],
              })( */}
              <Upload accept=".pdf" {...props}>
                  <Button><Icon type="upload" /> Upload</Button>
                </Upload>
              {/* )} */}
            </FormItem>
            <Form.Item
              {...formItemLayout}
              label="Tampilkan Dihalaman depan"
              // hasFeedback
            >
              {getFieldDecorator('ispublish', {
                valuePropName: 'checked',
                initialValue: true,
              })(<Checkbox>Publish</Checkbox>)}
            </Form.Item>
          </Form>
        </Modal>

        <Card className="gx-card" title="Daftar Landasan Hukum Internasional">
          {/* {isFetching ?
            <div className="gx-loader-view">
              <CircularProgress />
            </div> : null} */}
          <Row gutter={16}>
            <Col className="gutter-row" span={20}>
              {/* <SearchBox placeholder="Cari" /> */}
            </Col>
            <Col className="gutter-row" span={4}>
              <div style={{ marginBottom: 16 }}>
                <Button type="primary" onClick={this.showModal} block >
                  <Icon type="plus" /> Tambah
                </Button>
              </div>
            </Col>
          </Row>
          
          <Table 
            className="gx-table-responsive"
            loading={isFetching}
            onChange={this.handleChange}
            dataSource={(resultsLandasanHukum !== undefined ? resultsLandasanHukum.data : []) }
            columns={columns}
          />
        </Card>
        
        {/* {showMessage  ?
          message.error(JSON.stringify(alertMessage)) : null} */}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  http: state.http,
  isFetching: state.http.isFetching,
  resultsLandasanHukum: state.general.results,
  isSuccess: state.general.isSuccess,
  showMessage: state.general.showMessage,
  alertMessage: state.general.alertMessage,
  isLoading: state.general.loader,
})
const LandasanHukumInternasionals = Form.create()(LandasanHukumInternasional);
export default connect(mapStateToProps, {
  fetchLandasanHukumInt,
  setLandasanHukumInt,
  postLandasanHukumInt,
  showAuthLoader
})(LandasanHukumInternasionals);

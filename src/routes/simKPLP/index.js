import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import asyncComponent from "util/asyncComponent";

const simKPLP = ({match}) => (
  <Switch>
    <Route path={`${match.url}/tupoksi`} component={asyncComponent(() => import('./Tupoksi'))} />
    <Route path={`${match.url}/landasanhukumnasional`} component={asyncComponent(() => import('./LandasanHukumNasional'))} />
    <Route path={`${match.url}/landasanhukuminternasional`} component={asyncComponent(() => import('./LandasanHukumInternasional'))} />
    <Route path={`${match.url}/publikasi`} component={asyncComponent(() => import('./Publikasi'))} />
    <Route path={`${match.url}/maklumatpelayaran`} component={asyncComponent(() => import('./MaklumatPelayaran'))}/>
  </Switch>
);

export default simKPLP;

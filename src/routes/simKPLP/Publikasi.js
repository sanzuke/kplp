import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Button, Card, Form, Input, Radio, Select, Upload, Icon, Table, Divider, DatePicker } from "antd";
import { convertToRaw, EditorState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

const FormItem = Form.Item;
const Option = Select.Option;

const props = {
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  onChange({ file, fileList }) {
    if (file.status !== 'uploading') {
      console.log(file, fileList);
    }
  },
  defaultFileList: [
    {
      uid: '1',
      name: 'xxx.png',
      status: 'done',
      response: 'Server Error 500', // custom error message to show
      url: 'http://www.baidu.com/xxx.png',
    },
    {
      uid: '2',
      name: 'yyy.png',
      status: 'done',
      url: 'http://www.baidu.com/yyy.png',
    },
    {
      uid: '3',
      name: 'zzz.png',
      status: 'error',
      response: 'Server Error 500', // custom error message to show
      url: 'http://www.baidu.com/zzz.png',
    },
  ],
};

const dataSource = [
  {
    no: '1',
    kategori: 'Surat Edaran',
    perihal: "Lorem ipsum",
    deskripsi: "Lorem ipsum",
    file: 'file.pdf',
  },
  {
    no: '2',
    kategori: 'Intruksi',
    perihal: "Lorem ipsum",
    deskripsi: "Lorem ipsum",
    file: 'file.pdf',
  },
  {
    no: '3',
    kategori: 'Surat Edaran',
    perihal: "Lorem ipsum",
    deskripsi: "Lorem ipsum",
    file: 'file.pdf',
  },
  {
    no: '4',
    kategori: 'Telegram',
    perihal: "Lorem ipsum",
    deskripsi: "Lorem ipsum",
    file: 'file.pdf',
  },
];

const columns = [
  {
    title: 'No',
    dataIndex: 'no',
    key: 'no',
  },
  {
    title: 'Kategori',
    dataIndex: 'kategori',
    key: 'kategori',
  },
  {
    title: 'Perihal',
    dataIndex: 'perihal',
    key: 'perihal',
  },
  {
    title: 'Deskripsi / Narasi / Isi',
    dataIndex: 'deskripsi',
    key: 'deskripsi',
  },
  {
    title: 'File',
    dataIndex: 'file',
    key: 'file',
  },
  {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
      <span>
        <a href="javascript:;">view</a>
        <Divider type="vertical" />
        <a href="javascript:;">Ubah</a>
        <Divider type="vertical" />
        <a href="javascript:;">Hapus</a>
      </span>
    ),
  },
];

class Publikasi extends PureComponent {
  state = {
    editorState: EditorState.createEmpty(),
  };

  onEditorStateChange = (editorState) => {
    this.setState({
      editorState,
    });
  };

  handleFormLayoutChange = (e) => {
    this.setState({ formLayout: e.target.value });
  }

  onChange = (date, dateString) => {
    console.log(date, dateString)
  }

  constructor() {
    super();
    this.state = {
      formLayout: 'horizontal',
    };
  }

  render() {
    const { editorState } = this.state;
    const { formLayout } = this.state;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = formLayout === 'horizontal' ? {
      labelCol: { xs: 24, sm: 6 },
      wrapperCol: { xs: 24, sm: 14 },
    } : null;
    const buttonItemLayout = formLayout === 'horizontal' ? {
      wrapperCol: { xs: 24, sm: { span: 14, offset: 6 } },
    } : null;

    return (
      <div>
        <Card className="gx-card" title="Input Publikasi">
          <Form layout={formLayout}>

            <FormItem
              {...formItemLayout}
              label="Kategori Publikasi"
              hasFeedback
            >
              {getFieldDecorator('select', {
                rules: [
                  { required: true, message: 'Silahkan pilih Kategori publikasi' },
                ],
              })(
                <Select placeholder="Silahkan pilih Kategori publikasi">
                  <Option value="surat_edaran">Surat Edaran</Option>
                  <Option value="intruksi">Instruksi</Option>
                  <Option value="telegram">Telegram</Option>
                </Select>
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Tanggal Publikasi"
              hasFeedback
            >
              {getFieldDecorator('tanggal', {
                rules: [
                  { required: true, message: 'Silahkan pilih tanggal publikasi' },
                ],
              })(
                <DatePicker onChange = { this.onChange } />
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Perihal Publikasi"
              hasFeedback
            >
              {getFieldDecorator('perihal', {
                rules: [
                  { required: true, message: 'Silahkan isi perihal publikasi' },
                ],
              })(
                <Input type="text" />
              )}
            </FormItem>

            <FormItem {...formItemLayout}
              label="Deskripsi / Narasi / Isi"
              hasFeedback
            >
              {getFieldDecorator('deskripsi', {
                rules: [
                  { required: true, message: 'Silahkan isi Deskripsi' },
                ],
              })(
                <Editor editorStyle={{
                  width: '100%',
                  minHeight: 100,
                  borderWidth: 1,
                  borderStyle: 'solid',
                  borderColor: 'lightgray',

                }}
                  editorState={editorState}
                  wrapperClassName="demo-wrapper"
                  onEditorStateChange={this.onEditorStateChange}
                />
              )}
              {/* <textarea style={{ width: '100%', height: 200 }}
              disabled
              value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
            /> */}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Upload file"
              hasFeedback
            >
              {getFieldDecorator('select', {
                rules: [
                  { required: true, message: 'Upload file' },
                ],
              })(
                <Upload {...props}>
                  <Button><Icon type="upload" /> Upload</Button>
                </Upload>
              )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="Publis ke web KPLP?"
              hasFeedback
            >
              {getFieldDecorator('select', {
                // rules: [
                //   { required: true, message: 'Silahkan pilih kategori aturan' },
                // ],
              })(
                <Radio.Group name="radiogroup" defaultValue={1}>
                  <Radio value={1}>Ya</Radio>
                  <Radio value={2}>Tidak</Radio>

                </Radio.Group>
              )}
            </FormItem>

            <FormItem {...buttonItemLayout}>
              <Button type="primary">Update</Button>
            </FormItem>
          </Form>
        </Card>

        <Card className="gx-card" title="Daftar Publikasi">
          <Table dataSource={dataSource} columns={columns} />
        </Card>

      </div>
    );
  }
}

Publikasi.propTypes = {

};
const Publikasis = Form.create()(Publikasi);
export default Publikasis;

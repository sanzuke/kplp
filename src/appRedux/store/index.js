import {applyMiddleware, compose, createStore} from "redux";
import reducers from "../reducers/index";
import {routerMiddleware} from "react-router-redux";
import createHistory from "history/createBrowserHistory";
import createSagaMiddleware from "redux-saga";
import { all } from "redux-saga/effects";

// saga collection
import authSagas from "../sagas/Auth";
import generalSagas from "../general/saga";

import { persistStore, persistReducer } from 'redux-persist'
import immutableTransform from 'redux-persist-transform-immutable'
import storage from 'redux-persist/lib/storage'

// Initial state
// const initialState = {}

const history = createHistory();
const routeMiddleware = routerMiddleware(history);
const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware, routeMiddleware];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// Configure redux persist
const persistConfig = {
  debug: true,
  key: 'root',
  storage,
  transforms: [immutableTransform()],
  blacklist: ['http', 'users', 'groups'],
}
const persistedReducer = persistReducer(persistConfig, reducers)

export function* rootSaga() {
  yield all([
    authSagas(),
    generalSagas()
  ]);
}

export default function configureStore(initialState) {
  const store = createStore(persistedReducer, initialState,
    composeEnhancers(applyMiddleware(...middlewares)));
  // Create persistor
  const persistor = persistStore(store)

  sagaMiddleware.run(rootSaga);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers/index', () => {
      const nextRootReducer = require('../reducers/index');
      store.replaceReducer(nextRootReducer);
    });
  }
  // return store;
  return { store, persistor }
}

export {history};

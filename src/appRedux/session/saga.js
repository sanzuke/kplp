import {
  SESSION_FETCH, SESSION_SET_USER, SESSION_SET_TOKEN,
  SESSION_RESET, LOGIN, LOGOUT,
  MENU_ACTIVE_SET, MENU_RESET } from '../../constants/ActionTypes'
import { takeLatest, call, put } from 'redux-saga/effects'
import { httpRequest } from '../../appRedux/http/saga'
import url from '../../util/url'

export function* fetchSession () {
  // const user = yield call(httpRequest, {
  //   method: 'get',
  //   url: url('user/detail/'),
  // })
  const user = null
  if (user !== null) yield put({ type: SESSION_SET_USER, payload: user.results })
}

export function* login ({ email, password }) {
  console.log("2342434242");
  
  const data = yield call(httpRequest, {
    method: 'post',
    url: url('auth/login/'),
    data: {
      email,
      password,
    },
  })

  if (data !== null) {
    yield put({ type: SESSION_SET_TOKEN, payload: data })
    yield put({ type: MENU_ACTIVE_SET, payload: data.menu })
  }
}

export function* logout () {
  const data = yield call(httpRequest, {
    method: 'post',
    url: url('auth/logout/'),
    data: {},
  })

  if (data != null) {
    yield put({ type: SESSION_RESET })
    yield put({ type: MENU_RESET })
  }
}

export function* sessionWatcher () {
  yield takeLatest(SESSION_FETCH, fetchSession)
  yield takeLatest(LOGIN, login)
  yield takeLatest(LOGOUT, logout)
}

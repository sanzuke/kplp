import { SESSION_FETCH, SESSION_RESET, SESSION_SET_USER, SESSION_SET_TOKEN } from '../../constants/ActionTypes'

const initialState = {
  authenticated: false,
  token: null,
  created: null,
  expired: null,
  user: {
    nama_lengkap: null,
    email: null,
  },
  satuan_kerja: [],
  permissions: [],
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SESSION_RESET:
      return initialState
    case SESSION_FETCH:
    case SESSION_SET_TOKEN:
      if (payload) {
        return {
          ...state,
          authenticated: true,
          token: payload.token,
          created: payload.created,
          expired: payload.expired,
          user: payload.user,
          satuan_kerja: payload.satuan_kerja,
          permissions: payload.permissions,
        }
      }
    // eslint-disable-line no-fallthrough
    case SESSION_SET_USER:
      if (payload) {
        return {
          ...state,
          user: payload,
          updatedAt: Date.now(),
        }
      }
    // eslint-disable-line no-fallthrough
    default:
      return state
  }
}

const TOGGLE_DRAWER = 'TOGGLE_DRAWER'

const initialState = {
  drawerOpen: true,
}

export default (state = initialState, action) => {
  const { type } = action

  switch (type) {
    case TOGGLE_DRAWER:
      return {
        ...state,
        drawerOpen: !state.drawerOpen,
      }
    default:
      return state
  }
}

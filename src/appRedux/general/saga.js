import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import {
  TUPOKSI_POST,
  TUPOKSI_FETCH,
  TUPOKSI_SET,
  TUPOKSI_RESET,

  DIREKTORAT_FETCH,
  DIREKTORAT_SET,
  DIREKTORAT_RESET,

  SEKSI_FETCH,
  SEKSI_SET,
  SEKSI_RESET,

  LANHUK_FETCH,
  LANHUK_POST,
  LANHUK_SET
} from "./ActionTypes";
import { showAuthMessage, userSignInSuccess, userSignOutSuccess, userSignUpSuccess } from "../actions/Auth";
import {
  setDirektorat,
  setSeksi,
  setTupoksi,
  resetGeneral,
  setLandasanHukumInt,
  setPostLandasanHukum
} from "./actions"
import { httpRequest } from '../http/saga'
import url from '../../util/url'

function* fetchDirektoratRequest() {
  // const { seksi_id } = payload
  try {
    const response = yield call(httpRequest, {
      method: 'get',
      url: url('v1/general/subdit'),
      // data: {
      //   seksi_id
      // },
    });

    if(response) {
      yield put(setDirektorat(response));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

function* fetchSeksiRequest() {
  // const { seksi_id } = payload
  try {
    const response = yield call(httpRequest, {
      method: 'get',
      url: url('v1/general/seksi'),
      // data: {
      //   seksi_id
      // },
    });

    if (response) {
      yield put(setSeksi(response));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

function* postTupoksiRequest({ payload }) {
  // console.log(payload);
  // yield put(resetGeneral)
  try {
    const response = yield call(httpRequest, {
      method: 'post',
      url: url('v1/general/tupoksi'),
      data: payload,
    });

    if (response) {
      yield put(setTupoksi(response));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

// Landasan Hukum 
function* fetchLandasanHukumRequest(payload) {
  
  const { payload: { jenis } } = payload
  try {
    const response = yield call(httpRequest, {
      method: 'get',
      url: url('v1/general/landasan-hukum/show')+'?jenis='+jenis,
      data: {
        jenis
      }
    });
    
    if (response) {
      yield put(setLandasanHukumInt(response));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

function* postLandasanHukumRequest({ payload, callback }) {
  console.log(payload);
  let hasil = {}
  const { jenis } = payload
  const config = {
    headers: {
      'content-type': 'multipart/form-data',
    },
  };
  try {
    const response = yield call(httpRequest, {
      method: 'post',
      url: url('v1/general/landasan-hukum'),
      data: payload,
      header: config
    });

    if (response && callback) {
      yield put(setPostLandasanHukum(response));
    }
  } catch (error) {
    yield put(callback);
    // yield put(showAuthMessage(error));
  }
}


// ===================================================================
export function* fetchDirektorat() {
  yield takeEvery(DIREKTORAT_FETCH, fetchDirektoratRequest);
}

export function* fetchSeksi() {
  yield takeEvery(SEKSI_FETCH, fetchSeksiRequest);
}

export function* postTupoksi() {
  yield takeEvery(TUPOKSI_POST, postTupoksiRequest);
}

export function* fetchLandasanHukum() {
  yield takeEvery(LANHUK_FETCH, fetchLandasanHukumRequest);
}
export function* postLandasanHukum() {
  yield takeEvery(LANHUK_POST, postLandasanHukumRequest);
}

// ===================================================================
export default function* rootSaga() {
  yield all([
    fork(fetchSeksi),
    fork(fetchDirektorat),
    fork(postTupoksi),
    fork(fetchLandasanHukum),
    fork(postLandasanHukum),
  ]);
}
import {
  DIREKTORAT_FETCH,
  DIREKTORAT_SET,
  DIREKTORAT_RESET,

  SEKSI_FETCH,
  SEKSI_SET,
  SEKSI_RESET,

  TUPOKSI_POST,
  TUPOKSI_FETCH,
  TUPOKSI_SET,
  TUPOKSI_RESET,

  LANHUK_FETCH,
  LANHUK_SET,
  LANHUK_POST_SET,
  LANHUK_POST,
  LANHUK_RESET,
} from "./ActionTypes";

const INIT_STATE = {
  loader: false,
  alertMessage: '',
  showMessage: false,
  isSuccess: false,
  initURL: '',
  subditCollection: null,
  seksiCollection: null,
  results: {},
};

export default (state = INIT_STATE, { type, payload }) => {
  switch (type) {
    // Direktorat
    case DIREKTORAT_FETCH: {
      return {
        ...state,
        loader: false,
        subditCollection: payload
      }
    }
    case DIREKTORAT_SET: {
      return {
        ...state,
        loader: false,
        subditCollection: payload
      }
    }
    case DIREKTORAT_RESET: {
      return {
        ...state,
        subditCollection: payload
      }
    }

    // Seksi
    case SEKSI_FETCH: {
      return {
        ...state,
        loader: false,
        seksiCollection: payload
      }
    }
    case SEKSI_SET: {
      return {
        ...state,
        loader: false,
        seksiCollection: payload
      }
    }
    case SEKSI_RESET: {
      return {
        ...state,
        seksiCollection: payload
      }
    }

    case TUPOKSI_POST: {
      return {
        ...state,
        loader: false,
        results: payload
      }
    }
    case TUPOKSI_FETCH: {
      return {
        ...state,
        loader: false,
        results: payload
      }
    }
    case TUPOKSI_SET: {
      return {
        // ...state,
        loader: false,
        showMessage: true,
        isSuccess: true,
        alertMessage: payload.message,
        results: payload
      }
    }
    case TUPOKSI_RESET: {
      return {
        ...state,
      }
    }

    case LANHUK_FETCH: {
      return {
        ...state,
        loader: false,
        results: payload
      }
    }

    case LANHUK_POST_SET: {
      return {
        ...state,
        loader: false,
        showMessage: true,
        isSuccess: true,
        alertMessage: payload,
        
        // results: payload
      }
    }

    case LANHUK_SET: {
      return {
        ...state,
        loader: false,
        results: payload
      }
    }
    default:
      return state;
  }
}
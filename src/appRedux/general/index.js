import { all } from "redux-saga/effects";
import authSagas from "./saga";

export default function* rootSaga(getState) {
  yield all([
    authSagas()
  ]);
}

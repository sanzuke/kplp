import {
  DIREKTORAT_FETCH,
  DIREKTORAT_SET,
  DIREKTORAT_RESET,

  SEKSI_FETCH,
  SEKSI_SET,
  SEKSI_RESET,

  TUPOKSI_POST,
  TUPOKSI_FETCH,
  TUPOKSI_SET,
  TUPOKSI_RESET,

  LANHUK_FETCH,
  LANHUK_SET,
  LANHUK_POST_SET,
  LANHUK_POST,
  LANHUK_RESET,
} from "./ActionTypes";

export const fetchDirektorat = (payload) => {
  return {
    type: DIREKTORAT_FETCH,
    payload: payload
  };
};

export const setDirektorat = (payload) => {
  return {
    type: DIREKTORAT_SET,
    payload: payload
  };
};

export const fetchSeksi = (payload) => {
  return {
    type: SEKSI_FETCH,
    payload: payload
  };
};

export const setSeksi = (payload) => {
  return {
    type: SEKSI_SET,
    payload: payload
  };
};

export const postTupoksi = (data) => {
  return {
    type: TUPOKSI_POST,
    payload: data
  };
};

export const setTupoksi = (data) => {
  return {
    type: TUPOKSI_SET,
    payload: data
  };
};

export const resetGeneral = (data) => {
  return {
    type: TUPOKSI_RESET,
    payload: data
  };
};

export const fetchLandasanHukumInt = (data) => {
  return {
    type: LANHUK_FETCH,
    payload: data
  };
};

export const postLandasanHukumInt = (data, callback) => {
  return {
    type: LANHUK_POST,
    payload: data,
    callback
  };
};

export const setPostLandasanHukum = (data) => {
  return {
    type: LANHUK_POST_SET,
    payload: data
  };
}

export const setLandasanHukumInt = (data) => {
  return {
    type: LANHUK_SET,
    payload: data
  };
};

import { HTTP_REQUEST, HTTP_SUCCESS, HTTP_FAILURE, SESSION_RESET } from '../../constants/ActionTypes'
import { delay } from 'redux-saga'
import { takeLatest, call, put, select } from 'redux-saga/effects'
import request from '../../util/request'
let errorMsg = ''
const setAuthorizationHeader = (session, headers = {}) => {
  // const { authenticated, token } = session
  const authenticated = localStorage.getItem('keyApi')
  if (authenticated) {
    // Set Authorization header if authenticated
    // user detected in session 
    return {
      ...headers,
      // Authorization: authenticated, //`Bearer ${token}`,
      apikey: authenticated, //`Bearer ${token}`,
    }
  }

  return headers
}

export function* httpRequest (options) {
  console.log("masuk ke fungsi http request");
  
  try {
    // Set HTTP Request to track http state
    yield put({ type: HTTP_REQUEST })

    // Get Session from state
    const session = yield select((state) => state.session)

    // Set authorization headers from token in session
    const headers = yield call(setAuthorizationHeader, session, options.headers)

    // Define final headers for http request
    const finalOptions = {
      ...options,
      headers,
    }

    // Do http Request
    const response = yield call(request, finalOptions)
    // Set HTTP Success state after 3 seconds
    yield call(delay, 300)
    yield put({ type: HTTP_SUCCESS })
    // console.log(response)
    return response
  } catch (error) {
    if (error === 401) {
      yield put({ type: HTTP_FAILURE, payload: { error } })
      yield put({ type: SESSION_RESET })
    }
    // Set HTTP Error State after 3 seconds
    yield call(delay, 300)
    yield put({ type: HTTP_FAILURE, payload: error })
    errorMsg = error
    return errorMsg
  }

  return null
}

export function* httpFailure () {
  console.log(`An HTTP Error Ocurred : ${errorMsg}`) // eslint-disable-line no-console
}

export function* httpWatcher () {
  yield takeLatest(HTTP_FAILURE, httpFailure)
}

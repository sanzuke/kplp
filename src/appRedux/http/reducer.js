import {
  HTTP_REQUEST, HTTP_SUCCESS, HTTP_FAILURE,
  HTTP_RESET,
} from '../../constants/ActionTypes'

const initialState = {
  isFetching: false,
  error: false,
  errorMessage: null,
  receivedAt: null,
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case HTTP_RESET:
      return initialState
    case HTTP_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
        errorMessage: null,
        receivedAt: null,
      }
    case HTTP_SUCCESS:
      return {
        ...state,
        isFetching: false,
        error: false,
        errorMessage: null,
        receivedAt: Date.now(),
      }
    case HTTP_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
        // errorMessage: payload.error.response.data.error,
        errorMessage: payload.message,
        receivedAt: Date.now(),
      }
    default:
      return state
  }
}

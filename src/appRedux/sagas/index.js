import {all} from "redux-saga/effects";
import authSagas from "./Auth";
import generalSagas from "../general/saga";

export default function* rootSaga(getState) {
  yield all([
    authSagas(),
    generalSagas()
  ]);
}

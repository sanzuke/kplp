import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";
import Settings from "./Settings";
import Auth from "./Auth";

import global from '../global/reducer'
import http from '../http/reducer'
import session from '../session/reducer'
import general from '../general/reducer'


const reducers = combineReducers({
  routing: routerReducer,
  settings: Settings,
  auth: Auth,
  global,
  http,
  session,
  general,
});

export default reducers;
